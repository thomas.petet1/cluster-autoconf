# Deploy Pxe Server with diskless nodes automatically

### Welcome!

This project is aimed to quicly deploy diskless cluster nodes from a PXE server.
It allows you to keep an easily manageable architecture for your project, being able to add or remove packages on your nodes filesystem without having to create a new image each time.

Base architecture achieved with this project is the following:

![Architecture diagram](images/arch.jpg)

>In this diagram, the master server is also used to host our PXE server and NFSRoot. Of course, it would be better to use PXE services inside containers and to host NFSRoot under a dedicated NFS server (as [TrueNas Core](https://www.truenas.com/truenas-core) for example). That is why we will try to bring these features as soon as the base of this project will be done.

### Branches

In addition with the `Base` branch, two branches will be added to this project:
- The `HPC` branch:
	- Will provide a basic cluster configuration with [slurm](https://slurm.schedmd.com/overview.html) nodes and slurmctld/slurmdbd hosted on the master/PXE server;
- `Microservices` branch:
	- Will provide a basic [kubernetes](https://kubernetes.io) cluster configuration with kubctl hosted on the master/PXE server and nodes behind it;
	- Some solutions like [Rancher](https://rancher.com) may be used to ease kubernetes management.

## Let''s get started...

You can choose between one of three branches, depending on your needs.
To begin working with this project, you first have to clone this repository on the machine you plan to use as PXE server. 
To do so, you can run the following command in your terminal: 
`git clone https://gitlab.com/thomas.petet1/cluster-autoconf.git`

Once this is done, you can give a look at code and configurations but **DO NOT RUN IT IMMEDIATLY**.
Indeed, you will have to be patient and be sure you checked a few boxes to be sure your system is ready.

## Prerequisites

- Security:
	- Before running this script, you will have to disable selinux on your machine to avoid future conflicts when booting the nodes (we will try to fix this later but it is the easy way we found to be sure to get it working). This involves that you want to get your server in a safe environment;
	- BTW: **Maintainers of this code cannot be held responsible for any issue caused by these scripts and their prerequisites. If you encounter some bugs, it is nice to report it but since it may be specific to your systems and needs, we may be able to help you**.

- Network:
	- You want to be sure that you have access yo internet (which should be the case if you can clone this repository...) but also to be sure you can fetch all your required packages from your repository (if you cannot, you will first want to check if you did not mispelled a package or ask your administrator if it is available from your local repo);
	- Check if your interfaces are up and running and that their configuration matches the configuration contained in the [main.sh](main.sh) script;
	- The recommanded method is to place the "PXE" interface in an isolated environment, mainly to avoid dhcp conflicts;
	- You can also configure your network to setup nat setup for exemple. In this case, you have to know that firewalld is disabled when this script runs so you would have to re-enable it and configure it by yourself.
	
- OS:
	- This project was first made to run on CentOS 7.8 so you may have to adapt some commands and packages before running it if you plan to run on an other distro;
	- It is not planned to port it to debian like systems anytime soon but we will make further efforts to make it fully compatible with all RH like systems;
	- This has only been tested on x86_64 systems, some packages may not behave the same way on ARM systems. 

## Configuration and run

You will find some parameters at the top of the [main.sh](main.sh) script. Most of them are self-explanatory and they are all pre-filled with the configuration we used at testing.

The only things you may have to check are network, required packages and passwords

It is recommended to be root user to run this script. In a future version, we will introduce a specific user to slightly improve security.
Once you have entered your settings, you can run the script by typing `./main.sh`.
The script will mostly run by itself. The only times you may be asked to interact are the following:
- Error: If an error occures, you will be asked if you want to abort execution;
- SSH Key: If you have not already created a rsa key with ssh-keygen, you will have to create it during execution;
- Services check: at some point, services will be launched. To be sure there is no issue before finishing the run or at least to let you know if a service failed launching;
- In the HPC script, we had some issues with MySQL commands so we inserted a bash interface with a few instructions that the user has to run.

Once the script is finished, you will be able to chroot into your newly created NFSRoot, for example to change configurations or passwords or to enable/disable services at startup.

When all of it is done, you can connect your nodes to the same network and power them on.
Check if they are configured to boot from BIOS (since UEFI is not yet supported by this script so, unless you added some additionnal configurations, it will not work).
Your nodes may then handle it by themselves.
If you did not configure dhcp hosts in your dhcpd configuration, you may find your nodes IPs by checking dhcp leases or by scanning you network (with NMAP for example).
Since the nodes are meant to be in a cluster environment, selinux and firewalld are disabled by default on these.


