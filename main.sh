###############     Variables     ###############
#TODO: move variables to .env file

#Main script
nfshare=/share
nfsroot=/share/nfsroot
tftpath=/var/lib/tftpboot
sednfsroot=$(sed 's/\//\\\//g' <<< "$nfsroot")

masterpkgs="dhcp xinetd nfs-utils tftp-server openssh-server dnsmasq net-tools wget syslinux"
globalpkgs="wget"
liblist="gcc"

masterip="10.0.10.254"
masterhn="master-001"
initialpos=`pwd`

#DHCP conf:
subnet="10.0.10.0"
router="${masterip}"
nmask="255.255.255.0"
dname="clust.local"
dnservers=$router
pxeserver=$router
srange="10.0.10.100"
erange="10.0.10.150"

#dnsmasq conf:
dnsdomain=$dname

#files
dhcpconf="/etc/dhcp/dhcpd.conf"

###############     Variables     ###############


###############     Functions     ###############
errorhandler(){
    echo "$1"
    exit 0
}

selector(){
    while true; do
        read -p "$1" yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

nodeconf(){
chroot $nfsroot /bin/bash << EOF

    systemctl disable firewalld

EOF
}
###############     Functions     ###############


sestatus | grep disabled || errorhandler "Please disable selinux before running this script"
useradd dnsmasq
#disabling firewalld for practical reason, will enhance this later
systemctl stop firewalld
systemctl disable firewalld


###############     Packages installation     ###############
echo "Ensuring System packages are up to date and ready to deliver pxe booting"
yum update  -y  ||  errorhandler "Cannot fetch updates, preventing issues by exiting prematurely"
yum install epel-release -y
yum install $masterpkgs -y 
# yum groupinstall    "Infiniband Support" -y || \
    yum install rdma-core -y || \
    selector "Cannot find infiniband support, do you still want to continue execution?[yes/no] :"
yum install $liblist -y

echo "Creating nfsroot for readonly access by nodes"
if [ -d ${nfsroot} ] ; then
    selector "${nfsroot} already exists, do you want to overwrite it?[yes/no] (No will stop the script) :"
fi
rm -rf $nfsroot
mkdir -p $nfsroot
yum --installroot=$nfsroot groupinstall "Minimal Install" -y --releasever=/ || \
    yum --installroot=$nfsroot groupinstall "Base" -y --releasever=/ \
    errorhandler "cannot install Basic fs for nodes, aborting"

yum --installroot=$nfsroot groupinstall "Compute Node" -y --releasever=/ 2> /dev/null
yum --installroot=$nfsroot install epel-release -y
# yum --installroot=$nfsroot groupinstall "Infiniband Support" -y --releasever=/ || \
    yum --installroot=$nfsroot install rdma-core -y --releasever=/
yum --installroot=$nfsroot install $globalpkgs -y --releasever=/
yum --installroot=$nfsroot install $liblist -y --releasever=/
###############     Packages installation     ###############


###############     Configuring basic services     ###############
echo "Configuring nodes"
touch $nfsroot/etc/fstab
touch $nfsroot/etc/selinux/config
\cp nodescfgs/fstab    $nfsroot/etc/fstab
\cp nodescfgs/seconf   $nfsroot/etc/selinux/config

echo "Configuring master node"
hostnamectl set-hostname $masterhn
echo "copying config files"
\cp srvcfgs/dhcpd.conf            $dhcpconf
\cp srvcfgs/dnsmasq.conf          /etc/dnsmasq.conf
\cp srvcfgs/dnsmasq-dns.conf      /etc/dnsmasq-dns.conf
\cp srvcfgs/dnsmasq-hosts.conf    /etc/dnsmasq-hosts.conf
echo "
${masterip}   ${masterhn}" >> /etc/hosts

echo "applying parameters to configs"
#DHCP
sed -i "s/routertobe/${router}/g"       $dhcpconf
sed -i "s/nmasktobe/${nmask}/g"         $dhcpconf
sed -i "s/dntobe/${dname}/g"            $dhcpconf
sed -i "s/dnstobe/${dnservers}/g"       $dhcpconf
sed -i "s/pxservertobe/${pxeserver}/g"  $dhcpconf
sed -i "s/subnetobe/${subnet}/g"        $dhcpconf
sed -i "s/srangetobe/${srange}/g"       $dhcpconf
sed -i "s/erangetobe/${erange}/g"       $dhcpconf

#DNS
sed -i "s/dnsdomtobe/${dnsdomain}/" /etc/dnsmasq.conf


echo "Preparing master for tftpboot"
mkdir -p $tftpath
\cp /usr/share/syslinux/pxelinux.0           $tftpath
# \cp $nfsroot/boot/{vmlinuz*,initramfs*24*}   $tftpath
mkdir $tftpath/pxelinux.cfg
\cp /boot/vmlinuz-`uname -r`        $tftpath/vmlinuz
# \cp /boot/initramfs-`uname -r`.img  $tftpath/initramfs
\cp srvcfgs/pxe.conf                $tftpath/pxelinux.cfg/default
dracut -v -m "nfs network base" \
    --add-drivers "nfs nfsv4" \
    --install "mount mkdir" \
    --include ./srvcfgs/init / /var/lib/tftpboot/initramfs -f
chmod 755 -R $tftpath
sed -i "s/tftpservtobe/${masterip}/g" $tftpath/pxelinux.cfg/default
sed -i "s/nfspathtobe/${sednfsroot}/g" $tftpath/pxelinux.cfg/default
sed -i '/disable/c\disable = no' /etc/xinetd.d/tftp
#killing dnsmasq process to be sure port is free
#kind of brutal but this is the only way I found
dnsmasqid=`netstat -ltnp | grep -w ':53' | awk -F " " {'print $7'} | awk -F "/" {'print $1'}`
kill $dnsmasqid
systemctl   start   {dhcpd,nfs,tftp,xinetd,dnsmasq}
systemctl   status  {dhcpd,nfs,tftp,xinetd,dnsmasq}
selector    "Are all services running properly?[yes/no] :"
systemctl   enable  {dhcpd,nfs,tftp,xinetd,dnsmasq}


echo    "configuring NFS Shares"
mkdir   -p  $nfshare
echo    "###Following section was generated by an automated script###
${nfshare}  *(rw,no_root_squash,async)
${nfsroot}  *(ro,no_root_squash,async)
/home   *(rw,no_root_squash,async)
###end of section###" >> /etc/exports
###############     Configuring basic services     ###############


###############     Final Configuration     ###############
echo "generating ssh-key to enable automatic ssh connection to nodes"
ls      /root/.ssh/id_rsa.pub   ||  ssh-keygen
mkdir   -p  $nfsroot/root/.ssh
touch   $nfsroot/root/.ssh/authorized_keys
cat     /root/.ssh/id_rsa.pub >  $nfsroot/root/.ssh/authorized_keys

nodeconf || selector "Some issues have occured while configuring nodes, continue?[yes/no] :"

echo "You can now chroot into ${nfsroot} if you want to commit further modifications to the nodes' fs"

 ###############     Final Configuration     ###############





